export interface IKeyValue {
    key: number;
    value: string;
}
export class KeyValue {
    public key: number;
    public value: string;

    constructor(key: number = undefined,value: string = undefined) {
        this.key = key;
        this.value = value;
    }
}
export class KeyValuePair<T,R> {
    public key: T;
    public value: R;

    constructor(key: T = undefined,value: R = undefined) {
        this.key = key;
        this.value = value;
    }
}