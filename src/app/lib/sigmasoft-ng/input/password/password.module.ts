import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { PasswordDirective } from "./password.directive";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [PasswordDirective],
  exports: [PasswordDirective]
})
export class InputPasswordModule {}
