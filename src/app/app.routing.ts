import { NgModule } from '@angular/core';
import { Route, RouterModule } from '@angular/router';
import {LayoutComponent} from "./components/layout/layout.component";




const routes: Route[] = [
  {
    path: '',
    component:LayoutComponent,
    children: [
      { path: '', loadChildren: './components/site/site.module#SiteModule' },
      { path: 'user', loadChildren: './components/user/user.module#UserModule' },
    ]
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
