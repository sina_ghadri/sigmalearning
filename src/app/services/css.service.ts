import { Injectable } from '@angular/core';

@Injectable()
export class CssService {
  private _colors: string[] = ['yellow', 'orange', 'red', 'turquise', 'green', 'sky', 'blue', 'violet', 'black', 'gray'];
  constructor() { }

  public get colors(): string[] { return this._colors; }
  public get paddings(): number[] { 
    let values: number[] = [];
    for(let i = 0;i <= 100;i += 5) {
      values.push(i);
    }
    return values;
   }
   public get margins(): number[] { 
    let values: number[] = [];
    for(let i = 0;i <= 100;i += 5) {
      values.push(i);
    }
    return values;
   }
   public get fonts(): number[] { 
    let values: number[] = [];
    for(let i = 6;i <= 50;i += 1) {
      values.push(i);
    }
    return values;
   }
}
