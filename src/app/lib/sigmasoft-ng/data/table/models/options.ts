export class DataTableRowOptions {
    public selectable?: boolean = false;
    public selectedMultiple?: boolean = true;
    public collapseMultiple?: boolean = false;
    public editable: boolean = false;
    public deletable?: boolean = false;
    public insertable?: boolean = false;

    public static getDisableAll(): DataTableExportOptions {
        let obj = new DataTableExportOptions;
        obj.print = false;
        obj.excel = false;
        return obj;
    }
}
export class DataTableExportOptions {
    public print?: boolean = false;
    public excel?: boolean = true;

    public static getDisableAll(): DataTableExportOptions {
        let obj = new DataTableExportOptions;
        obj.print = false;
        obj.excel = false;
        return obj;
    }
}
export class DataTableColumnOptions {
    public sortable?: boolean = true;
    public movable?: boolean = true;
    public visibility?: boolean = true;

    public static getDisableAll(): DataTableColumnOptions {
        let obj = new DataTableColumnOptions;
        obj.sortable = false;
        obj.movable = false;
        obj.visibility = false;
        return obj;
    }
}
export class DataTableOptions {
    public rowOptions?: DataTableRowOptions = new DataTableRowOptions;
    public exportOptions?: DataTableExportOptions = new DataTableExportOptions;
    public columnOptions?: DataTableColumnOptions = new DataTableColumnOptions;

    public page?: number = 1;
    public sort?: number = 0;
    public ascending?: boolean = true;
    public limit?: number = 10;
    public limits?: number[] = [10, 15, 25, 50, 100];
    public language?: string = 'en';
    public responsive?: boolean = false;
    public limitation?: boolean = true;
    public savable?: boolean = true;
    public searchable?: boolean = true;
    public paging?: boolean = true;
    public grouping?: boolean = true;
    public messageTime?: number = 5000;

    public static getEmptyMode(): DataTableOptions {
        let obj = new DataTableOptions;
        obj.exportOptions = DataTableExportOptions.getDisableAll();
        obj.columnOptions = DataTableColumnOptions.getDisableAll();
        obj.savable = false;
        obj.limitation = false;
        return obj;
    }
}