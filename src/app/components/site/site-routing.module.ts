import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {CourseComponent} from "./course/course.component";
import {LessonComponent} from "./lesson/lesson.component";
import {ArticleComponent} from "./article/article.component";
import {ArticlesComponent} from "./articles/articles.component";
import {RegisterComponent} from "./register/register.component";
import {ResetPasswordComponent} from "./reset-password/reset-password.component";
import {ContactUsComponent} from "./contact-us/contact-us.component";
import {DiscussionsComponent} from "./discussions/discussions.component";
import {DiscussionComponent} from "./discussion/discussion.component";

const routes: Routes = [
      { path: '', component: HomeComponent },
      { path: 'course', component: CourseComponent },
      { path: 'lesson/:id', component: LessonComponent },
      { path: 'articles', component: ArticlesComponent },
      { path: 'article/:id', component: ArticleComponent },
      { path: 'register', component: RegisterComponent },
      { path: 'reset-password', component:ResetPasswordComponent },
      { path: 'contact-us', component:ContactUsComponent },
      { path: 'discussions', component:DiscussionsComponent },
      { path: 'discussion:id', component:DiscussionComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiteRoutingModule { }
