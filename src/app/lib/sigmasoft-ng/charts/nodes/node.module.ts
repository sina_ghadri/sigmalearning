import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PanelComponent } from './components/panel/panel.component';
import { NodeComponent } from './components/node/node.component';
import { ItemComponent } from './components/item/item.component';
import { IOComponent } from './components/io/io.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    PanelComponent,
    NodeComponent,
    ItemComponent,
    IOComponent
  ],
  exports: [PanelComponent],
})
export class NodeModule { }
