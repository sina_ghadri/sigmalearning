import { DataTableRow } from './row';
import { DataTableColumn, DataTableColumnSearchType } from './column';
import { isNumber } from "util";

export class DataTableFilter {
    public static search(rows: DataTableRow[],columns: DataTableColumn[]): DataTableRow[] {
        return rows.filter(row => {
            let count = 0;
            for (let i = 0; i < columns.length; i++) {
                let search = columns[i].search.value;
                if(search != null && search != undefined) {
                    let value = columns[i].getValue(row);
                    value = (value != null && value != undefined ? (isNumber(value) ? value.toString() : value) : '');
                    search = search.toString();
                    if (columns[i].search.enable) {
                        switch(columns[i].search.type)
                        {
                            case DataTableColumnSearchType.Text:
                                if(value.toLowerCase().indexOf(search.toLowerCase()) > -1) count++;
                                break;
                            case DataTableColumnSearchType.Select:
                                if(value.toLowerCase() == search.toLowerCase()) count++;
                                break;
                        }
                    }
                } else count++;
            }
            return columns.length == count;
        })
    }
    public static sort(rows: DataTableRow[], columns: DataTableColumn[],columnIndex: number,ascending: boolean): DataTableRow[] {
        columnIndex = (isNumber(columnIndex) ? columnIndex : 0);
        return rows.sort((n1, n2) => {
            let value1 = columns[columnIndex].getValue(n1);
            let value2 = columns[columnIndex].getValue(n2)
            if (value1 > value2) return (ascending ? 1 : -1);
            if (value1 < value2) return (ascending ? -1 : 1);
            return 0;
        });
    }
    public static paging(array: DataTableRow[],from: number,count: number): DataTableRow[] {
        let temp: DataTableRow[] = [];
        if(from > 0) {
            for (let i = from - 1, to = i + count, length = array.length; i < to && i < length; i++) temp.push(array[i]);
        }
        return temp;
    }
}