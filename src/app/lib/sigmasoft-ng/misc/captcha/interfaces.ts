import { Observable } from "rxjs";
import { OperationResult, KeyValuePair } from "../../../sigmasoft-ts";

export interface ICaptchaService {
    get(url: string): Observable<OperationResult<KeyValuePair<string, string>>>;
}