import { Component } from '@angular/core';
import { NotificationService } from './notification.service';

@Component({
    selector: 'ss-message-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})
export class NotificationComponent {
    
    constructor(public service: NotificationService) {}
}