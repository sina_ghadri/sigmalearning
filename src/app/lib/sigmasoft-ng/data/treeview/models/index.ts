export class TreeViewItem {
    public id: number = null;
    public text: string;
    public isOpen: boolean;
    public src: string;
    public items: TreeViewItem[];
    public parent: TreeViewItem;
    public parentId: number;

    public constructor(item:TreeViewItem = null) {
        this.id = (item ? item.id : null);
        this.src = (item ? item.src : null);
        this.text = (item ? item.text : null);
        this.items = (item ? item.items : []);
        this.parent = (item ? item.parent : null);
        this.parentId = (item ? item.parentId : null);
    }
    public isFile() { return this.items.length == 0; }
    public close() { this.isOpen = false; }
    public open() { 
        this.isOpen = true;
        if(this.parent) this.parent.open();
    }
}