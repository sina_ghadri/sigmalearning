import { isArray } from 'util';

export class Convert {
  static toObject<T>(value: any[], type?: new () => T): T[];
  static toObject<T>(value: any, type?: new () => T): T;

  static toObject<T>(value: any | any[], type?: new () => T): T | T[] {
    if (isArray(value)) {
      let array = [];
      for (let i = 0; i < value.length; i++) array.push(Convert.toObject(value[i], type));
      return array;
    } else {
      let obj;
      if (type) obj = new type;
      else obj = {};
      for (let k in value) obj[k] = value[k];
      return obj;
    }
  }
}