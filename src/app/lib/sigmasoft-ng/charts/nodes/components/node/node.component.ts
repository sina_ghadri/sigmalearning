import { Component, OnInit, Input, ElementRef, Output, EventEmitter, HostBinding } from '@angular/core';
import { Node, NodeItemIO, NodeItem, NodeItemInput, NodeItemOutput } from '../../models';

@Component({
  selector: 'ss-chart-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {
  @Input() node: Node;

  @Output('connecting') onconnecting: EventEmitter<NodeItemIO> = new EventEmitter;
  @Output('dragStart') ondragStart: EventEmitter<any> = new EventEmitter;
  @Output('dragEnd') ondragEnd: EventEmitter<any> = new EventEmitter;

  
  @Output('itemClick') onitemClick: EventEmitter<NodeItem> = new EventEmitter;
  @Output('itemDblClick') onitemDblClick: EventEmitter<NodeItem> = new EventEmitter;
  @Output('inputDblClick') oninputDblClick: EventEmitter<NodeItemInput> = new EventEmitter;
  @Output('outputDblClick') onoutputDblClick: EventEmitter<NodeItemOutput> = new EventEmitter;

  @HostBinding('style.left') get left(): string { return this.node.position.x + 'px'; }
  @HostBinding('style.top') get top(): string { return this.node.position.y + 'px'; }

  constructor(private element: ElementRef) {

  }

  ngOnInit() {
    this.node.element = this.element.nativeElement;
  }

  dragStart(e: MouseEvent, node: Node) {
    this.ondragStart.emit({ event: e, node: node });
  }
  dragEnd() {
    this.ondragEnd.emit();
  }
}
