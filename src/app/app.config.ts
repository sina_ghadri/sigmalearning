// let baseurl = 'http://identity.test-tts.ime.co.ir/api/admin/';
// let baseurl = 'http://localhost:9000/admin/';
let baseurl = 'http://172.16.56.105:9002/admin/';
let api = {
    url: baseurl,
    module: baseurl + 'module/',
    role: baseurl + 'role/',
    permission: baseurl + 'permission/',
    user: baseurl + 'user/',
    token: baseurl + 'token/',
    information: baseurl + 'information/',
    informationValue: baseurl + 'information-value/',
    menu: baseurl + 'menu/',
    cache: baseurl + 'cache/',
}
//Export Data
export const config = {
    login: 'http://login.test-tts.ime.co.ir',
    name: '',
    symbol: 'identity',
    api: {
        url: api.url,
        module: {
            url: api.module,
        },
        role: {
            url: api.role,
            permission: api.role + 'permission/',
            member: api.role + 'member/'
        },
        permission: {
            url: api.permission,
        },
        user: {
            url: api.user,
            role: api.user + 'role/',
            member: api.user + 'member/',
            information: api.user + 'information/',
            token: api.user + 'token/',
            password: api.user + 'password/'
        },
        token: {
            url: api.token,
        },
        information: {
            url: api.information,
        },
        informationValue: {
            url: api.informationValue,
        },
        menu: {
            url: api.menu,
        },
        cache: {
            url: api.cache,
        },
    }
};