import { Component, OnInit } from '@angular/core';
import { Panel } from 'sigmasoft-ng/panel/admin/models';
import { HttpConfigService } from 'sigmasoft-ng/misc/jwt/http-config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  exportAs: 'app-root',
})
export class AppComponent implements OnInit {
  panel: Panel = new Panel();
  public constructor(private httpConfig: HttpConfigService) {}
  ngOnInit(): void {
    let data = JSON.parse(localStorage.getItem('loginInfo'));
  }
}
