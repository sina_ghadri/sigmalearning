import { Component, OnInit, Input } from '@angular/core';
import { MultimediaGalleryItem } from './models';

@Component({
  selector: 'ss-multimedia-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss']
})
export class GalleryComponent implements OnInit {
  private _index: number;

  @Input() items: MultimediaGalleryItem[];
  @Input() 
  get index(): number { return this._index; };
  set index(value: number) { 
    if(this._index > this.items.length - 1) value = this.items.length - 1; 
    if(this._index != value) {
      this.show = false; 
      setTimeout(() => this.show = true,100);
    }
    this._index = value; 
  };

  get item(): MultimediaGalleryItem { return this.items[this.index]; }

  show: boolean;
  constructor() { }

  ngOnInit() {
    if(this.index == undefined) this.index = 0;
  }

  prev() { if(this._index < 1) this.index = this.items.length - 1; else this._index--; }
  next() { if(this._index > this.items.length - 2) this._index = 0; else this._index++; }
}
