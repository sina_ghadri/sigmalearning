import { Component, OnInit, forwardRef, Input, ElementRef, EventEmitter, Output, TemplateRef, HostListener } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'ss-input-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => DropdownComponent),
      multi: true
    }
  ]
})
export class DropdownComponent implements OnInit, ControlValueAccessor {
  @Input() ngClass: any = 'input-group';
  @Input() items: any[];
  @Input() multiple: boolean;
  @Input() label: string;
  @Input() arrow: boolean = true;
  @Input() template: TemplateRef<any>;
  @Input() search: string = '';

  @Input() compareWith: (x1: any, x2: any) => boolean;
  @Input() key: ((x: any) => number) | string;
  @Input() value: ((x: any) => string) | string = 'value';

  @Input()
  get valueMember(): ((x: any) => number) | string { return this.key; }
  set valueMember(value: ((x: any) => number) | string) { this.key = value; }

  @Input()
  get displayMember(): ((x: any) => string) | string { return this.value; }
  set displayMember(value: ((x: any) => string) | string) { this.value = value; }

  @Output()
  get change(): EventEmitter<any> { return this.onchange; }
  set change(value: EventEmitter<any>) { this.onchange = value; }

  @Output() onchange: EventEmitter<any> = new EventEmitter;
  @Output() onsearch: EventEmitter<string> = new EventEmitter;

  @HostListener('mousedown') onmouseDown() {
    this.open();
  }
  @HostListener('mouseup') onmouseUp() {
    this.open();
  }
  focusedIndex: number = 0;

  private _serchInterval: any;
  private _searchPreValue: string;
  private _values: any[];
  private _closingTimeout: any;
  private _onchange: (values: any | any[]) => any = () => { };
  private _ontouched: () => any = () => { };
  private _isdisabled: boolean;

  registerOnChange(fn: any): void { this._onchange = fn; }
  registerOnTouched(fn: any): void { this._ontouched = fn; }
  setDisabledState?(isdisabled: boolean): void { this._isdisabled = isdisabled; }

  get values(): any[] { if (!this._values) this._values = []; return this._values; }
  set values(value: any[]) { this._values = value; }

  constructor(private element: ElementRef) { }

  ngOnInit() {
    this._searchPreValue = this.search;
    if (this.multiple != undefined) this.multiple = true;
    if (!this.compareWith) this.compareWith = (x1, x2) => this.getValue(x1) == this.getValue(x2);
    if (this.label) this.ngClass = 'form-group';
  }
  getValue(x: any): any {
    if (!this.key) return x;
    if (typeof this.key == 'string') return x[this.key]
    return this.key(x);
  }
  getText(x: any): any {
    if (typeof this.value == 'string') return x[this.value];
    return this.value(x);
  }
  writeValue(obj: any | any[]): void {
    this.values = [];
    if (Array.isArray(obj)) {
      this.multiple = true;
      for (let i = 0, length = obj.length; i < length; i++) {
        let find = this.items.find(x => this.compareWith(x,obj[i]));
        if(find) this.values.push(find);
      }
    } else if (obj) {
        let find = this.items.find(x => this.compareWith(x,obj));
        if(find) this.values = [find];
    }
  }
  readValue() {
    let value;
    if (this.multiple) {
      value = [];
      for(let i = 0;i < this.values.length;i++) value.push(this.getValue(this.values[i]));
    }
    else value = (this.values.length > 0 ? this.getValue(this.values[0]) : null);
    this._onchange(value);
    this.onchange.emit(value);
  }
  getItems() {
    if (!this.items) return [];
    let items = this.items;
    if (this.search) {
      let searchValue = this.search.toLowerCase();
      items = items.filter(x => this.getText(x).toLowerCase().indexOf(searchValue) > -1);
    }
    return items;
  }

  isopen() { return this.element.nativeElement.classList.contains('open'); }
  open() { 
    this.element.nativeElement.classList.add('open'); 
    setTimeout(() => {
      if (this._closingTimeout) clearTimeout(this._closingTimeout); 
      this.focus();
    },1);
  }
  openIf() { if (!this.element.nativeElement.querySelector('.item:hover')) this.open(); }
  close() { this.element.nativeElement.classList.remove('open'); this._closingTimeout = null; }
  closeWithDelay() { this._closingTimeout = setTimeout(() => this.close(), 20); }
  toggle() { if (this.isopen()) this.close(); else this.open(); }
  focus() { let input = this.element.nativeElement.querySelector('input.search'); if (input) input.focus(); }
  addOrRemove(item: any) {
    if (this.multiple) {
      let flag = true;
      for (let i = 0, length = this.values.length; i < length; i++) if (this.compareWith(this.values[i], item)) { this.values.splice(i, 1); flag = false; break; }
      if (flag) this.values.push(item);
    }
    else {
      this.values = [item];
      this.close();
    }
    this.readValue();
    this.focus();
    if (this.multiple) this.open();
  }
  remove(item: any) {
    for (let i = 0, length = this.values.length; i < length; i++) if (this.compareWith(this.values[i], item)) { this.values.splice(i, 1); return; }
    this.readValue();
  }
  isActive(item: any): boolean {
    for (let i = 0, length = this.values.length; i < length; i++) if (this.compareWith(this.values[i], item)) return true;
    return false;
  }
  clear() {
    this.values = [];
    this.readValue();
  }
  keyup(e: KeyboardEvent) {
    if (this._searchPreValue != this.search) {
      if (this._serchInterval) clearTimeout(this._serchInterval);
      this._serchInterval = setTimeout(() => { this.onsearch.emit(this.search); }, 100)
      this._searchPreValue = this.search;
    }
  }
  keydown(e: KeyboardEvent) {
    if (e.keyCode >= 37 && e.keyCode <= 40) {
      this.open();
      e.preventDefault();
      if (e.keyCode == 38) this.focusedIndex--;
      if (e.keyCode == 40) this.focusedIndex++;
      let div = this.element.nativeElement.querySelector('.items');
      let li = div.querySelector(`li:nth-child(${this.focusedIndex})`);
      if (li) div.scrollTop = li.offsetTop;
    }

    let items = this.getItems();
    if (this.focusedIndex < 0) this.focusedIndex = 0;
    if (this.focusedIndex >= items.length) this.focusedIndex = items.length - 1;
    if (e.keyCode == 13) this.addOrRemove(items[this.focusedIndex]);
    if (e.keyCode == 27) this.close();
  }
}
