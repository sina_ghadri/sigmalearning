import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { ModalComponent } from "./components/modal/modal.component";
import { ModalConfirmComponent } from "./components/confirm/confirm.component";
import { ModalAlertComponent } from "./components/alert/alert.component";
import { ModalPromptComponent } from "./components/prompt/prompt.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [
    ModalComponent,
    ModalConfirmComponent,
    ModalAlertComponent,
    ModalPromptComponent
  ],
  exports: [
    ModalComponent,
    ModalConfirmComponent,
    ModalAlertComponent,
    ModalPromptComponent
  ]
})
export class OverlayModalModule {}
