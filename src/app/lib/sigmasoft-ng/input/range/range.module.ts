import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { RangeComponent } from "./range.component";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [RangeComponent],
  exports: [RangeComponent]
})
export class InputRangeModule {}
