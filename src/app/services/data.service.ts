import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { DataTableState } from '../lib/sigmasoft-ng';

@Injectable({ providedIn: 'root' })
export class DataService {

  constructor(private http: HttpClient) { }

  get(): Observable<any[]> {
    return this.http.get<any[]>('/api/data.json');
  }
  getWithDelay(delay: number): Observable<any[]> {
    return new Observable<any[]>(ob => {
      this.http.get<any[]>('/api/data.json').subscribe(data => {
        setTimeout(() => {
          ob.next(data);
          ob.complete();
        }, delay)
      }, e => ob.error(e));
    })
  }
  getLazyloading(delay: number, state: DataTableState): Observable<any> {
    return new Observable<any>(ob => {
      this.http.get<any[]>('/api/data.json').subscribe(data => {
        setTimeout(() => {
          data = data.filter(x => {
            for (let i = 0, length = state.searchBy.length; i < length; i++) {
              let value: string = x[state.searchBy[i].field];
              if (value && state.searchBy[i].search.value) {
                if (value.toString().toLowerCase().indexOf(state.searchBy[i].search.value.toString().toLowerCase()) < 0) return false;
              }
            }
            return true;
          });
          let count = data.length;
          data = data.sort((n1, n2) => {
            let value1 = n1[state.sortBy.field];
            let value2 = n2[state.sortBy.field]
            if (value1 > value2) return (state.ascending ? 1 : -1);
            if (value1 < value2) return (state.ascending ? -1 : 1);
            return 0;
          });
          data = data.slice((state.page - 1) * state.limit, state.page * state.limit);
          ob.next({
            count: count,
            data: data
          });
          ob.complete();
        }, delay)
      }, e => ob.error(e));
    })
  }
}
