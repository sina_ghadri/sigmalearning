import { Component, OnInit, forwardRef, Input, ElementRef, OnDestroy } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";

@Component({
  selector: "ss-input-range",
  templateUrl: "./range.component.html",
  styleUrls: ["./range.component.css"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RangeComponent),
      multi: true
    }
  ]
})
export class RangeComponent implements OnInit,OnDestroy, ControlValueAccessor {
	@Input() min: number = 0;
	@Input() max: number = 200;
	@Input() step: number = 1;
	@Input() color: string = '#0cb78a';

	isdisabled: boolean = false;
	onchange: (value: any) => any = value => {};
	ontouched: () => any = () => {};
	onmouseup: (e:MouseEvent) => any = (e: MouseEvent) => { this.follow = false; };
	onmousemove: (e:MouseEvent) => any = (e: MouseEvent) => { if(!this.follow) return; this.sliderChange(e); };

	_value: number;
	_percent: number;

	round(value: number): number {
		if(this.step > 0) {
			if(value%this.step < this.step/2) value -= value%this.step;
			else {
				value += this.step;
				value -= value%this.step;
			}
		}
		return value;
	}
	get value(): number { return this._value; }
	set value(value: number) {
		if(value < this.min) value = this.min;
		else if(value > this.max) value = this.max;
		
		this._value = this.round(value);
		let percent = (100 * value / (this.max - this.min));
		if(this._percent != percent) this.percent = percent;
	}
	get percent(): number { return this._percent; }
	set percent(percent: number) {
		if(percent < 0) percent = 0;
		else if(percent > 100) percent = 100;
		this._percent = percent;
		this.slider.style.left = this._percent + '%';

		let value = ((this.max - this.min) * percent / 100);
		if(this._value != value) this._value = this.round(value);
	}

	follow: boolean;

	bar: HTMLElement;
	slider: HTMLElement;

	constructor(private element: ElementRef) {}

	ngOnInit() {
		this.bar = this.element.nativeElement.querySelector('.bar');
		this.slider = this.element.nativeElement.querySelector('.slider');
		this.onmouseup

		this.element.nativeElement.addEventListener('mousedown',(e: MouseEvent) => { this.follow = true; this.sliderChange(e); });
		document.addEventListener('mouseup',this.onmouseup);
		document.addEventListener('mousemove',this.onmousemove);
	}
	ngOnDestroy(): void {
		document.removeEventListener('mouseup',this.onmouseup);
		document.removeEventListener('mousemove', this.onmousemove);
	}
	writeValue(value: any): void {
		this.value = value;
		this.onchange(this.value);
	}
	registerOnChange(fn: any): void { this.onchange = fn; }
	registerOnTouched(fn: any): void { this.ontouched = fn; }
	setDisabledState?(isdisabled: boolean): void { this.isdisabled = isdisabled; }

	sliderChange(e: MouseEvent) {
		var rect = this.element.nativeElement.getBoundingClientRect();
		this.percent = ((e.pageX - rect.left) /this.bar.offsetWidth) * 100;
		this.onchange(this.value);
	}
}
