import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Error404Module } from "./error-404/error-404.module";

@NgModule({
  imports: [CommonModule],
  exports: [
    Error404Module
  ],
  providers: []
})
export class BasePageModule {}
