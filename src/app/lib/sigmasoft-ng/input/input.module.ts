import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { CommonModule } from "@angular/common";

import { InputAutoCompleteModule } from "./auto-complete";
import { InputCheckboxModule } from "./checkbox";
import { InputChipsModule } from "./chips";
import { InputColorPickerModule } from "./color-picker";
import { InputDatepickerModule } from "./datepicker";
import { InputDropdownModule } from "./dropdown";
import { InputEditorModule } from "./editor";
import { InputFocusModule } from "./focus";
import { InputFormatterModule } from "./formatter";
import { InputMaskModule } from "./mask";
import { InputNextModule } from "./next";
import { InputNumberModule } from "./number";
import { InputPasswordModule } from "./password";
import { InputPatternModule } from "./pattern";
import { InputRadioModule } from "./radio";
import { InputRangeModule } from "./range";
import { InputRatingModule } from "./rating";
import { InputValidatorModule } from "./validator";

@NgModule({
  imports: [FormsModule, CommonModule],
  declarations: [],
  exports: [
    InputAutoCompleteModule,
    InputCheckboxModule,
    InputChipsModule,
    InputColorPickerModule,
    InputDatepickerModule,
    InputDropdownModule,
    InputEditorModule,
    InputFocusModule,
    InputFormatterModule,
    InputMaskModule,
    InputNextModule,
    InputNumberModule,
    InputPasswordModule,
    InputPatternModule,
    InputRadioModule,
    InputRangeModule,
    InputRatingModule,
    InputValidatorModule,
  ]
})
export class BaseInputModule {}
