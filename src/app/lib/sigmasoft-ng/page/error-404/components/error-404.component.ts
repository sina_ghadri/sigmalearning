import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
    selector: 'ss-panel-admin-error-404',
    templateUrl: './error-404.component.html',
    styleUrls: ['./error-404.component.scss'],
    providers: []
})
export class Error404Component implements OnInit {

    public constructor(private location: Location) { }
    ngOnInit(): void {
    }
    back() {
        this.location.back();
    }
}