import { Directive, OnInit, ElementRef, Input, HostListener, EventEmitter, Output } from '@angular/core';
import { NgModel } from '@angular/forms';

@Directive({
    selector: '[ss-input-mask]',
    providers: [NgModel],
})
export class MaskDirective implements OnInit {
    @Input('ss-input-mask') mask: any;

    @Output() input:EventEmitter<any> = new EventEmitter;

    patterns: any[] = [];
    backspace: boolean;

    public constructor(private element: ElementRef, private model:NgModel) { }

    ngOnInit(): void { 
        this.patterns = this.parseMask(this.mask);
        
        let value = this.element.nativeElement.value;
        if(value) setTimeout(() => this.writeValue(this.process(value).value),100);
    }
    
    @HostListener('keydown', ['$event']) keydown(e: KeyboardEvent) {
        let value = this.element.nativeElement.value;
        let result;
        if(e.keyCode == 8) { 
            //result = this.process(value + e.key);
            this.backspace = true; 
        } 
        else if(e.key.length == 1) { 
            e.preventDefault(); 
            value = this.process(value).value;
            result = this.process(value + e.key);
            this.backspace = false; 
        }
        if(result)
        {
            this.model.valueAccessor.writeValue(result.value);
            this.writeValue(result.value);
        }
    }
    writeValue(value: string) {
        this.input.emit({ target: { value: value } });
    }
    parseMask(mask: string | any[]): any[] {
        let patterns: any[] = [];
        if (typeof mask == 'string') {
            let backslash = false;
            for (let i = 0; i < this.mask.length; i++) {
                if (backslash) { patterns.push(this.mask[i]); backslash = false; continue; }
                if (this.mask.indexOf('yyyy') == i) { patterns.push({ length: 4, pattern: '^((1|2)|(1(2|3|4|8|9)|2(0|1))|(1(2|3|4|8|9)\\d|2(0|1)\\d)|(1(2|3|4|8|9)\\d{2}|2(0|1)\\d{2}))$' }); i += 3; }
                else if (this.mask.indexOf('MM') == i) { patterns.push({ length: 2, pattern: '^((0|1)|(0[1-9]|1[0-2]))$' }); i++; }
                else if (this.mask.indexOf('dd') == i) { patterns.push({ length: 2, pattern: '^((0|1|2|3)|(0[1-9]|(1|2)\\d)|3[0-1])$' }); i++; }
                else if (this.mask.indexOf('hh') == i) { patterns.push({ length: 2, pattern: '^(([0-2])|([0-1][0-9]|2[0-3]))$' }); i++; }
                else if (this.mask.indexOf('mm') == i) { patterns.push({ length: 2, pattern: '^(([0-5])|([0-5][0-9]))$' }); i++; }
                else if (this.mask.indexOf('ss') == i) { patterns.push({ length: 2, pattern: '^(([0-5])|([0-5][0-9]))$' }); i++; }
                else switch (this.mask[i]) {
                    case '9': patterns.push({ length: 1, pattern: '[0-9]' }); break;
                    case '8': patterns.push({ length: 1, pattern: '[0-8]' }); break;
                    case '7': patterns.push({ length: 1, pattern: '[0-7]' }); break;
                    case '6': patterns.push({ length: 1, pattern: '[0-6]' }); break;
                    case '5': patterns.push({ length: 1, pattern: '[0-5]' }); break;
                    case '4': patterns.push({ length: 1, pattern: '[0-4]' }); break;
                    case '3': patterns.push({ length: 1, pattern: '[0-3]' }); break;
                    case '2': patterns.push({ length: 1, pattern: '[0-2]' }); break;
                    case '1': patterns.push({ length: 1, pattern: '[0-1]' }); break;
                    case '0': patterns.push({ length: 1, pattern: '[0]' }); break;
                    case 'w': patterns.push({ length: 1, pattern: '\\w' }); break;
                    case 'W': patterns.push({ length: 1, pattern: '\\W' }); break;
                    case 'd': patterns.push({ length: 1, pattern: '\\d' }); break;
                    case 'D': patterns.push({ length: 1, pattern: '\\D' }); break;
                    case 's': patterns.push({ length: 1, pattern: '\\s' }); break;
                    case 'S': patterns.push({ length: 1, pattern: '\\S' }); break;
                    case 'b': patterns.push({ length: 1, pattern: '\\b' }); break;
                    case 'A': patterns.push({ length: 1, pattern: '[A-Z]' }); break;
                    case 'a': patterns.push({ length: 1, pattern: '[a-z]' }); break;
                    case '@': patterns.push({ length: 1, pattern: '[a-zA-Z]' }); break;
                    case '\\': backslash = true; break;
                    default: patterns.push(this.mask[i]); break;
                }
            }
        }
        else {
            for (let i = 0; i < mask.length; i++) {
                if (typeof mask[i] == 'string') patterns.push({ length: 1, pattern: mask[i] });
                else patterns.push(mask[i]);
            }
        }
        return patterns;
    }

    private process(value: string): { value: string, ismatch: boolean, segments: any[] } {
        let prevalue;
        let result: { value: string, ismatch: boolean, segments: any[] } = { value: '', ismatch: true, segments: [] };
        for (let i = 0, j = 0;i < this.patterns.length; i++) {
            let pattern = this.patterns[i];
            if(typeof pattern == 'string') {
                prevalue = result.value;
                result.value += pattern;
                j += pattern.length;
            } else if(j < value.length) {
                let length = this.patterns[i].length;
                let sub = value.substring(j, j + length);
                while (sub.length > 0 && !sub.match(pattern.pattern)) {
                    sub = sub.substring(0, sub.length - 1);
                    result.ismatch = false;
                }
                if (!sub) break;
                result.value += sub;
                prevalue = result.value;
                if(!result.ismatch || sub.length < length) {
                    result.ismatch = false;
                    break;
                }
                j += length;
            }
            else break;
        }
        
        if(this.backspace && !result.ismatch) result.value = prevalue;
        return result;
    }
}