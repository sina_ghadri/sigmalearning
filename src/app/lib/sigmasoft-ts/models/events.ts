export class ArrayEvents {
    public static push(arr,before: (obj: any) => boolean | void, after?: (obj: any) => void) {
        arr.push = function(e) {
            let result = before(e);
            if(result || result == undefined)
            {
                Array.prototype.push.call(arr, e);
                if(after) after(e);
            }
        }
    }
    public static pop(arr, before: (obj: any) => boolean | void, after?: (obj: any) => void) {
        arr.pop = function() {
            if (arr.length > 0) {
                let e = arr[arr.length - 1];
                let result = before(e)
                if(result || result == undefined)
                {
                    result = Array.prototype.pop.call(arr);
                    if(after) after(e);
                    return result;
                }
            }
        }
    }
}