import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ConfigurationService {
  private _configuration: any;
  get configuration(): any {
    if(this._configuration) return this.configuration;
    console.error('First Call loadConfiguration Method');
  }

  constructor(private http: HttpClient) { }

  loadConfiguration(url: string): Observable<any> {
    if(this._configuration) return new Observable(ob => ob.next(this._configuration));
    return new Observable(ob => {
      this.http.get(url).subscribe(config => {
        this.refactorConfigurationData(config);
        this._configuration = config;
        ob.next(config); 
      });
    }) 
  }
  private refactorConfigurationData(config: any, insiderConfig?: any) {
    if (!insiderConfig) insiderConfig = config;
    let regex = new RegExp("\{\{[0-9a-zA-Z\.]*\}\}");
    let keys = Object.keys(insiderConfig);
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let value = insiderConfig[key];
      if (typeof value == 'string') {
        let matches = regex.exec(value);
        if(matches) {
          for (let j = 0; j < matches.length; j++) {
            let expression = matches[j];
            expression = expression.substr(2, expression.length - 4);
            let replacement = '';
            eval('replacement = config.' + expression);
            insiderConfig[key] = value.replace(matches[j], replacement);
          }
        }
      }
    }
    for (let i = 0; i < keys.length; i++) {
      let key = keys[i];
      let value = insiderConfig[key];
      if (typeof value == 'object') {
        this.refactorConfigurationData(config, value);
      }
    }
  }
}
