import { NgModule } from "@angular/core";
import { MessageMessageModule } from "./message";
import { MessageNotificationModule } from "./notification";

@NgModule({
    imports: [

    ],
    exports: [
        MessageMessageModule,
        MessageNotificationModule,
    ]
})
export class BaseMessageModule {}