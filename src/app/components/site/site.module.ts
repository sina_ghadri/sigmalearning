import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SiteRoutingModule } from './site-routing.module';
import { HomeComponent } from './home/home.component';
import { CourseComponent } from './course/course.component';
import { LessonComponent } from './lesson/lesson.component';
import { ArticlesComponent } from './articles/articles.component';
import { ArticleComponent } from './article/article.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { DiscussionComponent } from './discussion/discussion.component';
import { DiscussionsComponent } from './discussions/discussions.component';

@NgModule({
  imports: [
    CommonModule,
    SiteRoutingModule
  ],
  declarations: [HomeComponent, CourseComponent, LessonComponent, ArticlesComponent, ArticleComponent, ContactUsComponent, LoginComponent, RegisterComponent, ResetPasswordComponent, DiscussionComponent, DiscussionsComponent]
})
export class SiteModule { }
