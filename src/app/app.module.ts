import { CommonModule } from "@angular/common";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

// import { SigmaSoftModule } from "sigmasoft-ng";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from "./app.routing";
import { AppComponent } from "./app.component";
import { HttpService } from 'sigmasoft-ng/misc/jwt/http.service'
import { HttpConfigService } from "sigmasoft-ng/misc/jwt/http-config.service";
import { CssService } from "./services/css.service";
import { DataService } from './services/data.service';
import {SigmaSoftModule} from './lib/sigmasoft-ng';
import { LayoutComponent } from './components/layout/layout.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { NewsComponent } from './components/layout/news/news.component';
import { HeaderComponent } from './components/layout/header/header.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    SigmaSoftModule
  ],
  declarations: [AppComponent, LayoutComponent, FooterComponent, NewsComponent, HeaderComponent],
  providers: [
    HttpConfigService,
    CssService,

    DataService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
