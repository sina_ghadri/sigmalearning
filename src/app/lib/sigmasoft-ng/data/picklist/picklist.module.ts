import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PicklistComponent } from './picklist.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PicklistComponent],
  exports: [PicklistComponent]
})
export class DataPicklistModule { }
