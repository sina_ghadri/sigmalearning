import { ArrayEvents } from "../../../../sigmasoft-ts/models/events";
import { Cache } from "../../../../sigmasoft-ts/models/cache";

export class Point {
    x: number = 0;
    y: number = 0;

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    add(point: Point): Point {
        this.x += point.x;
        this.y += point.y;
        return this;
    }
    sub(point: Point): Point {
        this.x -= point.x;
        this.y -= point.y;
        return this;
    }
}
export class Size {
    width: number = 0;
    height: number = 0;

    constructor(width: number = 0, height: number = 0) {
        this.width = width;
        this.height = height;
    }
}
Point.prototype.toString = function () {
    return this.x + ',' + this.y;
}
export class NodePanel {
    private _nodes: Node[] = [];
    private _cache: Cache = new Cache;

    element: HTMLElement;
    get position(): Point { let offset = this.element.getBoundingClientRect(); return new Point(offset.left, offset.top); }
    get scroll(): Point { return new Point(this.element.scrollLeft, this.element.scrollTop); }
    get size(): Size { let element = (<HTMLElement>this.element); return new Size(element.scrollWidth, element.scrollHeight); }

    get nodes(): Node[] { return this._nodes; }

    constructor() {
        ArrayEvents.push(this._nodes, (item: Node) => {
            item.panel = this;
        })
    }
    get relations(): NodeItemIORelation[] {
        return this._cache.get('relations', () => {
            let result: NodeItemIORelation[] = [];
            for (let i = 0; i < this.nodes.length; i++) {
                let node = this.nodes[i];
                for (let j = 0; j < node.items.length; j++) {
                    let item = node.items[j];
                    if (item.output) {
                        for (let k = 0; k < item.output.relations.length; k++) {
                            let relation = item.output.relations[k];
                            relation.path = this.getRelationPath(relation);
                            result.push(relation);
                        }
                    }
                }
            }
            return result;
        });
    }
    getRelationPath(relation: NodeItemIORelation): string {
        if (relation.output.element && relation.input.element) {
            let start = relation.output.position;
            let end = relation.input.position;
            let scroll = this.scroll;
            return this.getPath(new Point(start.x + scroll.x, start.y + scroll.y), new Point(end.x + scroll.x, end.y + scroll.y));
        }
    }
    getPath(start: Point, end: Point): string {
        let points: Point[] = [];
        let deltaPoint = new Point((end.x - start.x) / 3, (end.y - start.y) / 3)

        points.push(start);
        points.push(new Point(start.x + deltaPoint.x * 2 + 10, start.y));
        points.push(new Point(start.x + deltaPoint.x * 1 - 10, end.y));
        points.push(end);

        let path = '';
        let offset = this.position;
        for (let i = 0; i < points.length; i++) {
            if (i == 0) path += 'M';
            points[i].x -= offset.x;
            points[i].y -= offset.y;
            path += points[i].x.toFixed(1) + ',' + points[i].y.toFixed(1);
            if (i + 1 < points.length) path += ' ';
            if (i == 0) path += ' C';
        }
        return path;
    }
}
export class Node {
    private _name: string;
    private _position: Point = new Point;
    private _items: NodeItem[] = [];

    panel: NodePanel;
    element: HTMLElement;

    get position(): Point { return this._position; }
    set position(value: Point) { this._position = value; }

    get globalPosition(): Point { return this.position.sub(this.panel.position.add(this.panel.scroll)) }

    get name(): string { return this._name ? this._name : '[No Name]'; }
    set name(value: string) { this._name = value; }

    get items(): NodeItem[] { return this._items; }

    constructor(name?: string) {
        this._name = name;
        ArrayEvents.push(this._items, (item: NodeItem) => {
            item.node = this;
        })
    }
}

export class NodeItem {
    private _name: string;
    private _input: NodeItemInput;
    private _output: NodeItemOutput;

    element: HTMLElement;
    node: Node;

    get input(): NodeItemInput { return this._input; }
    set input(value: NodeItemInput) { if (value) value.nodeItem = this; this._input = value; }

    get output(): NodeItemOutput { return this._output; }
    set output(value: NodeItemOutput) { if (value) value.nodeItem = this; this._output = value; }

    get name(): string { return this._name ? this._name : '[No Name]'; }
    set name(value: string) { this._name = value; }

    constructor(name?: string) {
        this._name = name;
    }
}
export abstract class NodeItemIO {
    private _relations: NodeItemIORelation[] = [];

    element: HTMLElement;
    nodeItem: NodeItem;
    visible: boolean = true;
    disabled: boolean;
    selected: boolean;
    multiple: boolean;

    get position(): Point {
        let startPos = this.element.getBoundingClientRect();
        return new Point(startPos.left + 5, startPos.top + 5);
    }
    get relations(): NodeItemIORelation[] { return this._relations; }

    constructor() {
        ArrayEvents.push(this._relations, (item: NodeItemIORelation) => {
            if (this instanceof NodeItemOutput) {
                if (this._relations.find(x => x.input == item.input)) return false;
                if (!this.multiple && this._relations.length > 0) this._relations.pop();
            }
            else {
                if (this._relations.find(x => x.output == item.output)) return false;
                if (!this.multiple && this._relations.length > 0) this._relations.pop();
            }
            return true;
        }, (item: NodeItemIORelation) => {
            if (this instanceof NodeItemOutput) item.input._relations.push(item);
            else item.output._relations.push(item);
        });
        ArrayEvents.pop(this._relations, (item: NodeItemIORelation) => {
            let rel;
            if (this instanceof NodeItemOutput) {
                rel = item.input._relations.find(x => x.output == this);
                if (rel) item.input._relations.splice(item.input._relations.indexOf(rel), 1);
            } else {
                rel = item.output._relations.find(x => x.input == this);
                if (rel) item.output._relations.splice(item.output._relations.indexOf(rel), 1);
            }
        });
    }

    public getClasses(): string[] {
        let classes = [];
        if (this.disabled) classes.push('disabled');
        return classes;
    }

    public connect(item: NodeItemIO): NodeItemIORelation {
        let rel: NodeItemIORelation;
        if (this instanceof NodeItemOutput && item instanceof NodeItemInput) {
            rel = new NodeItemIORelation(this, item);
            this._relations.push(rel);
        }
        else if (this instanceof NodeItemInput && item instanceof NodeItemOutput) {
            rel = new NodeItemIORelation(item, this);
            item._relations.push(rel);
        }
        return rel;
    }
    public disconnect(item: NodeItemIO) {

    }
}
export class NodeItemInput extends NodeItemIO {

    constructor() {
        super();
    }
}
export class NodeItemOutput extends NodeItemIO {

    constructor() {
        super();
        this.multiple = true;
    }
}
export class NodeItemIORelation {
    private _output: NodeItemOutput;
    private _input: NodeItemInput;
    private _cache: Cache = new Cache;

    name: string = 'Relation';
    path: string;
    selected: boolean;

    get output(): NodeItemIO { return this._output; }
    get input(): NodeItemIO { return this._input; }

    get center(): Point {
        return this._cache.get('center', () => {
            let start = this.output.position;
            let end = this.input.position;
            let panelPos = this.input.nodeItem.node.panel.position;
            return new Point(start.x - panelPos.x + (end.x - start.x) / 2, start.y - panelPos.y + (end.y - start.y) / 2);
        });
    }

    get classes(): string[] {
        let arr: string[] = ['relation'];
        if(this.selected) arr.push('selected');
        return arr;
    }

    constructor(input: NodeItemInput, output: NodeItemOutput) {
        this._output = input;
        this._input = output;
    }

}
